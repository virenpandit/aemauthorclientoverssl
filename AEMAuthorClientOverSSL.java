import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
 
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.HttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.conn.ssl.*;
import java.security.cert.X509Certificate;
import org.apache.http.conn.ssl.*;
import org.apache.http.conn.scheme.*;
import org.apache.http.conn.*;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.impl.conn.*;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.protocol.HttpContext;
import org.apache.http.conn.ClientConnectionManager;
// import org.apache.http.client.utils.URIBuilder;

// Note: USES Apache Http Libraries version 4.1 and JDK>1.7.72 ONLY!!!

public class AEMAuthorClientOverSSL {

    String HOSTNAME = "CHANGE_ME_AEM_AUTHOR_SERVER_HOSTNAME";
    String CQ_USER = "viren";
    String CQ_PASSWORD = "lynx";
    int intPort = 443;

    String TARGET_URL = "https://CHANGE_ME_AEM_AUTHOR_SERVER_HOSTNAME/content/ramada/en_us/home.json";

    public static void main(String args[]) {
        new AEMAuthorClientOverSSL().connect();
    }
    
    public void connect() {
        try {
            // Use default JDK keystore (<JDK>\lib\security\cacerts) OR -
            // Use a custom certificate-store using command: keytool -genkey -alias mydomain -keyalg RSA -keystore keystore.jks -keysize 2048
            // Import the self-signed certificate into the keystore (JDK-default store or custom)
            //   keytool -import -noprompt -trustcacerts -alias "CHANGE_ME_AEM_AUTHOR_SERVER_HOSTNAME" -file base64cert.cer -keystore C:\apps\programs\SSLHttpClient\keystore.jks
        
            // Uncomment these 2 lines if using a custom certificate-store
            //System.setProperty("javax.net.ssl.trustStore", "C:\\apps\\programs\\SSLHttpClient\\keystore.jks");
            //System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

            SSLContext sslContext = SSLContext.getInstance("SSL");

            // Use a TrustManager that trusts everything
            sslContext.init(null, new TrustManager[] { new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                                System.out.println("getAcceptedIssuers ============= Skip");
                                return null;
                        }

                        public void checkClientTrusted(X509Certificate[] certs,
                                        String authType) {
                                System.out.println("checkClientTrusted ============= Ok");
                        }

                        public void checkServerTrusted(X509Certificate[] certs,
                                        String authType) {
                                System.out.println("checkServerTrusted ============= Ok");
                        }
            } }, new SecureRandom());
            TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] certificate, String authType) {
                    return true;
                }
            };
            //SSLSocketFactory sf = new SSLSocketFactory(sslContext);
            SSLSocketFactory sf = new SSLSocketFactory(acceptingTrustStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            Scheme httpsScheme = new Scheme("https", 443, sf);
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(httpsScheme);
            ClientConnectionManager cm = new SingleClientConnManager(schemeRegistry);
            // ClientConnectionManager cm = new PoolingClientConnectionManager(schemeRegistry);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(cm);
            //defaultHttpClient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, false);
            //defaultHttpClient.getParams().setParameter(ClientPNames.HANDLE_REDIRECTS, false);
            //defaultHttpClient.getParams().setParameter(ClientPNames.REJECT_RELATIVE_REDIRECT, false);
            //defaultHttpClient.getParams().setParameter(ClientPNames.MAX_REDIRECTS, 500);
            // defaultHttpClient.getParams().setParameter(ClientPNames.HANDLE_AUTHENTICATION, true);
            // Set the value for the 'User-Agent' HTTP Request Header
            defaultHttpClient.getParams().setParameter("http.useragent", "Apache HTTPComponents");

            MyRedirectHandler handler = new MyRedirectHandler();
            defaultHttpClient.setRedirectHandler(handler);
  
            PrintWriter out;

            // Control connection parameters instead of leaving them to defaults
            HttpParams params = new BasicHttpParams();
            params.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000); // 5 seconds socket connection timeout
            params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 10000); // 10 seconds max wait for response from server, after establishing socket connection
            // params.setBooleanParameter(CoreConnectionPNames.SO_KEEPALIVE, false);
            // params.setBooleanParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, true);
            // params.setParameter("http.protocol.allow-circular-redirects", true);
            // params.setParameter("http.protocol.handle-redirects", true);
            // params.setParameter("http.protocol.max-redirects", 50);
            // params.setParameter("http.protocol.handle-authentication", false);
            // params.setParameter("http.protocol.reject-relative-redirect", false);

            defaultHttpClient.setParams(params);

            // Provide the authentication credentials
            defaultHttpClient.getCredentialsProvider().setCredentials(new AuthScope(HOSTNAME,intPort), new UsernamePasswordCredentials(CQ_USER, CQ_PASSWORD));
             
            // URIBuilder uriBuilder = new URIBuilder();
            // Remember to remove URL encodings in the URL
            // See http://www.w3schools.com/tags/ref_urlencode.asp
            //uriBuilder.setScheme("https").setHost(HOSTNAME).setPort(intPort).setPath("/content/ramada/en_us/home.json");
            // /system/console/jmx/com.adobe.granite:type=QueryStat");
            //URI uri = null;
            //uri = uriBuilder.build();
            
            HttpGet httpGet = new HttpGet(TARGET_URL);
            
            System.out.println("Executing HTTP GET request " + httpGet.getURI());
            
            // Create a response handler
            // http://hc.apache.org/httpcomponents-client-ga/httpclient/apidocs/org/apache/http/impl/client/BasicResponseHandler.html
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
             
            // Try connecting to CQ
            String strResponseBody = defaultHttpClient.execute(httpGet, responseHandler);
            System.out.println(strResponseBody);
            // Perform other actions with the returned page content in strResponseBody
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    
    class MyRedirectHandler extends DefaultRedirectHandler {

        public URI lastRedirectedUri;

        @Override
        public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
            System.out.println("Inside isRedirectRequested()...");
            return super.isRedirectRequested(response, context);
        }

        @Override
        public URI getLocationURI(HttpResponse response, HttpContext context)
                throws ProtocolException {

            System.out.println("Inside getLocationURI()...");
            lastRedirectedUri = super.getLocationURI(response, context);

            return lastRedirectedUri;
        }
    }
}
