Demonstrates connecting to an AEM (5.6.1) Author server over SSL and using AEM authentication.

Depends on Apache Http library 4.1 and JDK 1.7_72+

Running -
1. Change the source-file, replacing CHANGE_ME_AEM_AUTHOR_SERVER_HOSTNAME with your AEM Hostname
2. <add the 2 jar-files to classpath>
3. javac AEMAuthorClientOverSSL.java
4. java AEMAuthorClientOverSSL
